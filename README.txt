#Overview
Provides login integration with http://auth0.com (paid subscription needed!)

From the auth0 website:

> [auth0 provides] Zero friction identity infrastructure, built for developers...A cloud service, APIs and tools that eliminate the friction of identity for your applications and APIs

This module integrates the auth0 signin widget with Drupal to act as an alternative single-sign-on solution.

#Features
Allows site visitors to authenticate with any connected provider using auth0's beautiful and awesome widget (https://docs.auth0.com/login-widget2)
Creates a Drupal user (linked by email address) so that Drupal roles/permission can be applied as needed

#Requirements
Requires the libraries (http://drupal.org/project/libraries) module, and the auth0 SDK. Use of the auth0 service also requires a paid subscription although they have a free trial.

#Installation

- Download and enable this module
- Install the auth0 SDK (https://docs.auth0.com/php-tutorial). Should result in "sites/[yoursite]/libraries/auth0_sdk/autoload.php"
- Configure your application & connections at auth0
- Configure your auth0 settings at /admin/config/people/auth0
- Visit /user to view the buttons for logging in with any connected identity providers. Note, an example block is also included that will open the login widget with no default provider selected.

#FAQ

- **Can a user link another provider after they have authenticated?** Yes, if they visit their drupal account page, they will see buttons to connect any non-connected providers.
- **Can I restrict access to certain users?** Yes, using auth0's rules would probably be the easiest route.
- **Will updates to a user's connected profiles update the Drupal user?** Yes and No. At this time, only the name, email, and data properties of the user object are synced. So if you define custom fields (or profile fields) that you want to track (ie First Name, Last Name), you will have to hook in and update those upon each login
