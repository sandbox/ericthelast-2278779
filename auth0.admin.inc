<?php
/**
 * @file
 * Admin settings file for Auth0 module.
 */

/**
 * Administration form for Auth0.
 */
function auth0_admin_form() {

  $form = array();

  $form['auth0_domain'] = array(
    '#title' => t('Auth0 Domain'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => variable_get('auth0_domain', ''),
  );

  $form['auth0_client_id'] = array(
    '#title' => t('Auth0 Client ID'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => variable_get('auth0_client_id', ''),
  );

  $form['auth0_client_secret'] = array(
    '#title' => t('Auth0 Client Secret'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => variable_get('auth0_client_secret', ''),
  );

  $form['auth0_connections'] = array(
    '#type' => 'textarea',
    '#title' => t('Auth0 Connections'),
    '#description' => t('Enter all of the connections that will be available for login (one per line). The connection name should match "Name" defined in your auth0 admin.'),
    '#default_value' => variable_get('auth0_connections'),
  );

  return system_settings_form($form);
}
