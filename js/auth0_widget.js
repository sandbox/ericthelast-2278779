(function($) {
  Drupal.behaviors.auth0_widget = {
    attach: function(context, settings) {
      var auth0_settings = settings.auth0;

      $("[data-auth0][data-action=login]").once().bind('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        var connections = $(this).data('auth0')
          , options = {};

        if (connections.length > 0) {
          options.connections = connections.split(',');
          options.enableReturnUserExperience = false;
        }

        var widget = getAuth0Widget(auth0_settings);
        widget.signin(options);
      })

      $("[data-auth0][data-action=link]").once().bind('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        var d = $(this).data();

        var widget = getAuth0Widget(auth0_settings);
        widget.signin({
          popup: true,
          connection: d.auth0,
          access_token: d.accesstoken,
        });
      })
    }
  }

  /**
   * Returns an instantiated Auth0 Widget.
   *
   * Using a new Auth0 Widget is needed since subsequent usages of the widget
   * are subject to the initial instantiation options.
   */
  function getAuth0Widget(settings) {
    return widget = new Auth0Widget({
      domain: settings.domain,
      clientID: settings.clientID,
      callbackURL: settings.callbackURL,
    });
  }
}(jQuery));
