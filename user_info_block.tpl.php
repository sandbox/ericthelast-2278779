<?php

/**
 * @file
 * Sample custom login template for Auth0 module.
 */

 ?>
<div class="row">
  <div class="twelve columns">
    <div class="panel secondary radius" style="text-align: right; margin: 20px 0; padding: 10px 15px;">
      <?php if(!$user_info): ?>
        <button class="button radius tiny" data-auth0 data-action="login">Login</button>
      <?php else: ?>
        <span Welcome <strong><?php echo check_plain($user_info['name']); ?></strong>!</span>
        <?php echo l(t("Log out"), "user/logout", array("attributes" => array("class" => "button radius tiny"))); ?>
      <?php endif ?>
    </div>
  </div>
</div>
