<?php

/**
 * @file
 * Include file for Auth0 module.
 */

use Auth0SDK\Auth0;

/**
 * Returns instantiated auth0 instance.
 */
function _auth0_get_client() {
  global $base_url;
  $path = libraries_get_path('auth0_sdk');
  require_once $path . '/autoload.php';

  $client = new Auth0(array(
    'domain'        => variable_get('auth0_domain'),
    'client_id'     => variable_get('auth0_client_id'),
    'client_secret' => variable_get('auth0_client_secret'),
    'redirect_uri'  => $base_url . '/auth0/callback',
  ));

  return $client;
}

/**
 * Implements callback().
 */
function auth0_callback() {
  global $user;
  $auth0 = auth0_get_client();

/*
  $auth0->setDebugger(function($info) {
    file_put_contents("php://stdout", sprintf("\n[%s] %s:%s [%s]: %s\n",
      date("D M j H:i:s Y"),
      ip_address(),
      $_SERVER["REMOTE_PORT"],
      "---",
      $info
    ));
  });
*/

  $token = $auth0->getAccessToken();

  if ($token) {
    $user_info = $auth0->getUserInfo();
    $_SESSION['auth0_access_token'] = $token;

    user_external_login_register($user_info['email'], 'auth0');
    $data = array_merge((array) $user->data, array('auth0' => $user_info));
    user_save($user, array('mail' => $user_info['email'], 'data' => $data));

    drupal_goto('');
  }
  else {
    return "<strong>Error while retrieving Auth0 Access Token</strong>";
  }
}
